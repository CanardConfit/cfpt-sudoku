﻿using System.Collections.Generic;

namespace TestSudoku
{
    public abstract class ISudokuData
    {
        protected List<int[,]> _possibiliteRowSolution, _possibiliteColSolution, _possibiliteCubeSolution;
        protected int[,] _sudoku, _sudokuSolution;

        public List<int[,]> PossibiliteRowSolution => _possibiliteRowSolution;

        public List<int[,]> PossibiliteColSolution => _possibiliteColSolution;

        public List<int[,]> PossibiliteCubeSolution => _possibiliteCubeSolution;

        public int[,] Sudoku => _sudoku;

        public int[,] SudokuSolution => _sudokuSolution;
    }
}