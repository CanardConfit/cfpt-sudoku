﻿using System.Text;
using NUnit.Framework;
using Sudoku;

namespace TestSudoku
{
    public class DataTest
    {
        private SudokuApi _sudoku;

        private ISudokuData _data;
        
        [SetUp]
        public void SetUp()
        {
            _data = new SudokuDifficileData();
            _sudoku = new SudokuApi(_data.Sudoku);
            _sudoku.Init();
            _sudoku.ReadDatas();
        }

        [Test]
        public void assert_possibiliteeRow_Valid()
        {
            for (int col = 0; col < _sudoku.Size; col++)
            {
                for (int row = 0; row < _sudoku.Size; row++)
                {
                    int val = _sudoku.PossibiliteRow[col, row];

                    int[,] list = _data.PossibiliteRowSolution[row];

                    StringBuilder sb = new StringBuilder();

                    foreach (int e in list)
                        sb.Append(e + ",");
                    
                    if (val != 0)
                        Assert.AreNotEqual(-1, SudokuUtils.Contains(list.Length, list, 0, val), $"ROW = La valeur {val} a été trouvée dans la ligne {row} mais on attendait {sb}");
                }
            }
        }
        
        [Test]
        public void assert_possibiliteeColumn_Valid()
        {
            for (int col = 0; col < _sudoku.Size; col++)
            {
                for (int row = 0; row < _sudoku.Size; row++)
                {
                    int val = _sudoku.PossibiliteColumn[row, col];

                    int[,] list = _data.PossibiliteColSolution[col];

                    StringBuilder sb = new StringBuilder();

                    foreach (int e in list)
                        sb.Append(e + ",");
                    
                    if (val != 0)
                        Assert.AreNotEqual(-1, SudokuUtils.Contains(list.Length, list, 0, val), $"COLUMN = La valeur {val} a été trouvée dans la colonne {col} mais on attendait {sb}");
                }
            }
        }
        
        [Test]
        public void assert_possibiliteeCube_Valid()
        {
            for (int cubeRow = 0; cubeRow < _sudoku.Div; cubeRow++)
            {
                for (int cubeCol = 0; cubeCol < _sudoku.Div; cubeCol++)
                {
                    for (int index = 0; index < _sudoku.Size; index++)
                    {
                        int val = _sudoku.PossibiliteCube[cubeRow, cubeCol, index];

                        int[,] list = _data.PossibiliteCubeSolution[cubeRow * 3 + cubeCol];

                        StringBuilder sb = new StringBuilder();

                        foreach (int e in list)
                            sb.Append(e + ",");
                    
                        if (val != 0)
                            Assert.AreNotEqual(-1, SudokuUtils.Contains(list.Length, list, 0, val), $"CUBE = La valeur {val} a été trouvée dans le cube {cubeRow}:{cubeCol} mais on attendait {sb}");
                    }
                }
            }
        }
    }
}