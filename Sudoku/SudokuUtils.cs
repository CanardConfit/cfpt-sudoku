﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Sudoku
{
    public static class SudokuUtils
    {
        public static int GetLastKey<T>(int size, T nullElement, T[,] list, int col)
        {
            for (int row = 0; row < size; row++)
                if (list[col,row].Equals(nullElement))
                    return row;
            return -1;
        }
        public static int GetLastKey<T>(int size, T nullElement, T[,,] list, int row, int col)
        {
            for (int index = 0; index < size; index++)
                if (list[col,row,index].Equals(nullElement))
                    return index;
            return -1;
        }
        public static int Contains<T>(int size, T[,] list, int col, T element)
        {
            for (int row = 0; row < size; row++)
                if (list[col,row].Equals(element))
                    return row;

            return -1;
        }
        public static int Contains<T>(int size, T[,,] list, int row, int col, T element)
        {
            for (int index = 0; index < size; index++)
                if (list[col,row,index].Equals(element))
                    return index;

            return -1;
        }
        
        public static List<int> Join(int size, int[,,] possibiliteCube, int row, int col, int[,] first, int indexFirst, int[,] second, int indexSecond)
        {
            List<int> toRet = new List<int>();

            for (int ifirst = 0; ifirst < size; ifirst++)
            {
                int element = first[indexFirst, ifirst];
                
                if (element == 0) continue;
                
                if (Contains(size, second, indexSecond, element) != -1 && Contains(size, possibiliteCube, row, col, element) != -1)
                    toRet.Add(element);
            }
            return toRet;
        }

        public static int[,] CreateTable(int size, TextBox[,] textBoxes)
        {
            int[,] value = new int[size, size];

            for (int x = 0; x < size; x++)
            for (int y = 0; y < size; y++)
                value[x, y] = textBoxes[x, y].Text == "" ? 0 : Convert.ToInt32(textBoxes[x, y].Text);
            
            return value;
        }

        public static int FindRow<T>(int size, List<T>[,] list, int col, T element)
        {
            for (int row = 0; row < size; row++)
                if (list[col, row] != null)
                    foreach (T el in list[col, row])
                        if (element.Equals(el))
                            return row;
            return -1;
        }
        public static int FindCol<T>(int size, List<T>[,] list, int row, T element)
        {
            for (int col = 0; col < size; col++)
                if (list[col, row] != null)
                    foreach (T el in list[col, row])
                        if (element.Equals(el))
                            return col;
            return -1;
        }
        public static KeyValuePair<int,int> FindCube<T>(int sizeCube, List<T>[,] list, int rowCube, int colCube, T element)
        {
            int startRow = rowCube * sizeCube;
            int startCol = colCube * sizeCube;
            
            for (int col = startCol; col < startCol + sizeCube; col++)
                for (int row = startRow; row < startRow + sizeCube; row++)
                    if (list[col, row] != null)
                        foreach (T el in list[col, row])
                            if (element.Equals(el))
                                return new KeyValuePair<int, int>(col, row);
            return new KeyValuePair<int, int>(-1, -1);
        }
    }
}