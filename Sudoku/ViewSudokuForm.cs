﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sudoku
{
    public partial class ViewSudokuForm : Form
    {
        private int size;
        private const int caseSize = 75;
        private const int baseTable = 10;
        private TextBox[,] textBoxes;

        // Facile
        private static int[,] tmpSudokuFacile = {
            {6,0,0,0,0,0,8,1,5},
            {1,9,0,0,8,5,0,0,7},
            {0,0,2,4,1,0,3,0,6},
            {5,7,0,0,0,8,0,6,2},
            {0,0,0,7,0,4,0,0,0},
            {3,6,0,9,0,0,0,7,4},
            {7,0,9,0,4,1,6,0,0},
            {4,0,0,8,7,0,0,3,9},
            {2,8,6,0,0,0,0,0,1},
        };
        // Moyen
        private static int[,] tmpSudokuMoyen = {
            {0,0,0,0,0,0,2,0,4},
            {6,0,0,0,0,7,0,1,0},
            {9,4,7,1,0,0,8,0,6},
            {0,0,0,3,0,0,6,9,1},
            {1,9,0,6,0,5,0,3,2},
            {7,6,3,0,0,1,0,0,0},
            {4,0,6,0,0,8,9,2,7},
            {0,7,0,4,0,0,0,0,5},
            {2,0,8,0,0,0,0,0,0},
        };
        
        // Difficile
        private static int[,] tmpSudokuDifficile = {
            {0,0,0,0,0,0,2,8,9},
            {0,0,0,4,0,9,0,0,6},
            {0,3,0,0,6,7,4,0,1},
            {0,0,0,0,0,0,9,0,4},
            {0,6,0,0,0,0,0,1,0},
            {7,0,3,0,0,0,0,0,0},
            {3,0,6,5,8,0,0,4,0},
            {5,0,0,6,0,4,0,0,0},
            {4,1,2,0,0,0,0,0,0},
        };
        
        // Dialolique
        private static int[,] tmpSudokuDiabolique = {
            {0,9,0,0,0,8,0,0,0},
            {0,0,0,0,0,0,2,0,6},
            {7,0,0,3,4,2,9,0,0},
            {0,0,0,0,0,0,0,0,0},
            {0,0,5,2,0,0,0,0,0},
            {3,0,8,0,6,0,1,9,0},
            {5,0,0,7,0,3,0,4,0},
            {0,0,0,0,0,0,0,0,0},
            {0,1,6,0,0,0,0,5,0},
        };
        
        private int[,] tmpSudoku = tmpSudokuDifficile;

        private SudokuApi _sudoku;
        private int[,] _values;

        public ViewSudokuForm() : this(9) { }

        public ViewSudokuForm(int size)
        {
            this.size = size;
            InitializeComponent();
            Paint += form_Paint;

            textBoxes = new TextBox[size, size];
            
            createGrid();

            _values = SudokuUtils.CreateTable(size, textBoxes);

            _sudoku = SudokuApi.Resolve(_values);
            
            Print();
        }


        private void Print()
        {
            PrintNormal();
            if (_sudoku.ExitedWithoutAll)
                PrintDebug();
        }
        
        private void PrintNormal()
        {
            //Console.WriteLine($"Time Total : {(DateTime.Now - _curTime).TotalMilliseconds} ms");

            for (int col = 0; col < size; col++)
            {
                for (int row = 0; row < size; row++)
                {
                    int v = _sudoku.Values[col, row];

                    if (_sudoku.Changes.Contains(col + ":" + row))
                    {
                        textBoxes[col,row].ForeColor = Color.Green;
                    }

                    textBoxes[col,row].Text = v != 0 ? v.ToString() : "";
                }
            }
        }
        private void PrintDebug()
        {
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    int v = _sudoku.Values[row, col];

                    if (v == 0)
                    {
                        writeDebugCase(textBoxes, String.Join(";", _sudoku.PossibiliteCase[row,col]), row, col);
                    }
                }
            }
        }
        
        private void form_Paint(object sender, PaintEventArgs e)
        {
            drawGrid(e.Graphics);
        }

        public static void writeDebugCase(TextBox[,] textBoxes, string str, int x, int y)
        {
            textBoxes[x, y].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, 0);
            textBoxes[x, y].ForeColor = Color.Red;
            textBoxes[x, y].Text = str;
        }
        
        private void drawGrid(Graphics g)
        {
            int racine = (int)Math.Sqrt(size);
            ClientSize = new Size((size * caseSize) + 20, (size * caseSize) + 20);

            for (int y = 0; y <= racine; y += 1)
            {
                g.DrawLine(new Pen(Color.Black, 5), new Point(baseTable, baseTable + y * racine * caseSize), new Point(caseSize * size + baseTable, baseTable + y * racine * caseSize));
            }
            for (int x = 0; x <= racine; x += 1)
            {
                g.DrawLine(new Pen(Color.Black, 5), new Point(caseSize * x * racine + baseTable, baseTable), new Point(caseSize * x * racine + baseTable, caseSize * size + baseTable));
            }
        }

        private void createGrid()
        {
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    TextBox tbx = new TextBox();
                    tbx.Font = new Font("Microsoft Sans Serif", 45F, FontStyle.Regular, GraphicsUnit.Point, 0);
                    tbx.Location = new Point(caseSize * x + baseTable, caseSize * y + baseTable);
                    tbx.Name = "tbx-" + x + "-" + y;
                    tbx.Text = tmpSudoku[y, x] != 0 ? tmpSudoku[y, x].ToString() : "";
                    tbx.Margin = new Padding(0);
                    tbx.Size = new Size(caseSize, caseSize);
                    tbx.TextChanged += tbx_TextChanged;
                    tbx.TextAlign = HorizontalAlignment.Center;
                    textBoxes[x,y] = tbx;
                    Controls.Add(tbx);
                }
            }
        }

        private void tbx_TextChanged(object sender, EventArgs e) {
            /*TextBox tbx = sender as TextBox;

            if (tbx.Text.Length > 1) {
                tbx.Text = tbx.Text.Substring(0, 1);
            }

            if (tbx.Text != "" && !Char.IsDigit(tbx.Text[0]))
                tbx.Text = "";*/
        }
    }
}
