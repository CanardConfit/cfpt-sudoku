﻿using System;
using System.Collections.Generic;

namespace Sudoku
{
    public class SudokuApi
    {
        private List<string> _changes;
        private int _size;
        private int _div;
        private bool _exitedWithoutAll;
        private int[,] _values;
        
        private int[,] _possibiliteRow;
        private int[,] _possibiliteColumn;
        private int[,,] _possibiliteCube;
        private List<int>[,] _possibiliteCase;
        private int _changement;

        public List<string> Changes => _changes;

        public int Size => _size;

        public int Div => _div;

        public bool ExitedWithoutAll => _exitedWithoutAll;

        public int[,] Values => _values;

        public int[,] PossibiliteRow => _possibiliteRow;

        public int[,] PossibiliteColumn => _possibiliteColumn;

        public int[,,] PossibiliteCube => _possibiliteCube;

        public List<int>[,] PossibiliteCase => _possibiliteCase;

        public bool Resolu
        {
            get
            {
                for (int row = 0; row < _size; row++)
                {
                    for (int col = 0; col < _size; col++)
                    {
                        if (_possibiliteColumn[row, col] != 0 || _possibiliteRow[row, col] != 0)
                            return false;
                    }
                }

                return true;
            }
        }
        
        /// <summary>
        /// Constructeur du Sudoku.
        /// </summary>
        /// <param name="values">Tableaux de données contenant le sudoku.</param>
        public SudokuApi(int[,] values)
        {
            _values = values;
        }

        /// <summary>
        /// Fonction d'initialisation de la résolution du sudoku.
        /// </summary>
        public void Init()
        {
            _size = (int)Math.Sqrt(_values.Length);
            _div = (int)Math.Sqrt(_size);

            _changes = new List<string>();
            
            _possibiliteRow = new int[_size,_size];
            _possibiliteColumn = new int[_size,_size];
            _possibiliteCube = new int[_div,_div,_size];
        }

        /// <summary>
        /// Fonction qui Inverse les données des lignes, colonnes et cube (impossibilitées à possibilitées).
        /// </summary>
        public void InvertDatas()
        {
            // Enlevement des impossibilitées dans les listes de lignes / colonnes
            for(int i = 0; i < _size; i++)
            {
                for (int i2 = 1; i2 <= _size; i2++)
                {
                    int index = SudokuUtils.Contains(_size, _possibiliteRow, i, i2);
                    if (index != -1) _possibiliteRow[i, index] = 0;
                    else _possibiliteRow[i, SudokuUtils.GetLastKey(_size, 0, _possibiliteRow, i)] = i2;

                    index = SudokuUtils.Contains(_size, _possibiliteColumn, i, i2);
                    if (index != -1) _possibiliteColumn[i, index] = 0;
                    else _possibiliteColumn[i, SudokuUtils.GetLastKey(_size, 0, _possibiliteColumn, i)] = i2;
                    
                    index = SudokuUtils.Contains(_size, _possibiliteCube, i % 3, i / 3, i2);
                    if (index != -1) _possibiliteCube[i / 3, i % 3, index] = 0;
                    else _possibiliteCube[i / 3, i % 3, SudokuUtils.GetLastKey(_size, 0, _possibiliteCube, i % 3, i / 3)] = i2;
                }
            }
        }
        
        /// <summary>
        /// Fonction qui remplie les tableaux de données avec les impossibilitées des lignes, colonnes et cubes.
        /// </summary>
        public void ReadDatas()
        {
            // Récuperation des possibiliées des lignes / colonnes
            for (int col = 0; col < _size; col++)
            {
                for (int row = 0; row < _size; row++)
                {
                    int cubeCol = col / _div;
                    int cubeRow = row / _div;

                    int valRow = _values[row, col];
                    int valCol = _values[col, row];
                    int valCube = _values[col, row];
                    
                    _possibiliteCube[cubeCol, cubeRow, SudokuUtils.GetLastKey(_size, 0, _possibiliteCube, cubeRow, cubeCol)] = valCube;

                    _possibiliteRow[col, row] = valRow;
                    _possibiliteColumn[col, row] = valCol;
                }
            }
        }

        /// <summary>
        /// Fonction qui créer les données de chaque case en fonction des lignes, colonnes et cubes.
        /// </summary>
        public void CalcCases()
        {
            _possibiliteCase = new List<int>[_size, _size];

            // Set des possibilitées par cases
            for (int col = 0; col < _size; col++)
                for (int row = 0; row < _size; row++)
                    if (_values[col, row] == 0)
                        _possibiliteCase[col, row] = SudokuUtils.Join(_size, _possibiliteCube, row / _div, col / _div, _possibiliteColumn, col, _possibiliteRow, row);
        }

        /// <summary>
        /// Fonction qui permet d'appliquer un changement sur un case
        /// </summary>
        /// <param name="num">Chiffre a mettre dans la case</param>
        /// <param name="row">Ligne de la case</param>
        /// <param name="col">Colonne de la case</param>
        public void MakeChange(int num, int row, int col)
        {
            _changement += 1;
            _values[col, row] = num;
            _changes.Add(col + ":" + row);
                                
            int index = SudokuUtils.Contains(_size, _possibiliteColumn, col, num);
            if (index != -1) _possibiliteColumn[col, index] = 0;
                                
            index = SudokuUtils.Contains(_size, _possibiliteRow, row, num);
            if (index != -1) _possibiliteRow[row, index] = 0;
                                
            index = SudokuUtils.Contains(_size, _possibiliteCube,  row / _div,col / _div, num);
            if (index != -1) _possibiliteCube[col / _div, row / _div, index] = 0;
        }
        
        /// <summary>
        /// Fonction qui permet de résoudre le sudoku.
        /// </summary>
        public void Resolve()
        {
            // Récuperation des données dans les listes
            ReadDatas();
            
            // Inversion des impossibilitées en possibilitées
            InvertDatas();
            
            // Boucle pour résoudre le sudoku
            while (!Resolu)
            {
                // Calcul des possibilitées par case
                CalcCases();

                _changement = 0;
                
                int[,,,] possibiliteUniqueCub = new int[_div, _div, _size, 1];

                for (int col = 0; col < _size; col++)
                {
                    int[,] possibiliteUniqueCol = new int[_size, 1];
                    int[,] possibiliteUniqueRow = new int[_size, 1];
                    
                    for (int row = 0; row < _size; row++)
                    {
                        if (_possibiliteCase[col, row] != null)
                        {
                            List<int> pos = _possibiliteCase[col, row];
                            
                            if (pos.Count == 1)
                            {
                                MakeChange(pos[0], row, col);
                            }
                            // Ajout des occurences des chiffres dans les possibilitées
                            foreach (int chiffre in pos)
                            {
                                possibiliteUniqueCol[chiffre - 1, 0] += 1;
                                
                                // Pour le cube
                                possibiliteUniqueCub[col / _div, row / _div, chiffre - 1, 0] += 1;
                            }
                        }
                        
                        // Inversion de row et col pour obtenir les cases de la ligne et non de la colonne
                        if (_possibiliteCase[row, col] != null)
                        {
                            List<int> pos = _possibiliteCase[row, col];
                            
                            foreach (int chiffre in pos)
                                possibiliteUniqueRow[chiffre - 1, 0] += 1;
                        }
                    }
                    // Trouver une seule occurence dans les possibilitées des colonnes pour la remplir
                    for (int chiffre = 0; chiffre < _size; chiffre++)
                    {
                        if (possibiliteUniqueCol[chiffre, 0] == 1)
                        {
                            int row = SudokuUtils.FindRow(_size, _possibiliteCase, col, chiffre + 1);
                            if (row != -1)
                                MakeChange(chiffre + 1, row, col);
                        }
                    }
                    int rowVirtual = col;
                    
                    // Trouver une seule occurence dans les possibilitées des lignes pour la remplir
                    for (int chiffre = 0; chiffre < _size; chiffre++)
                    {
                        if (possibiliteUniqueRow[chiffre, 0] == 1)
                        {
                            int colVirtual = SudokuUtils.FindCol(_size, _possibiliteCase, rowVirtual, chiffre + 1);
                            if (!Changes.Contains(colVirtual + ":" + rowVirtual) && colVirtual != -1)
                                MakeChange(chiffre + 1, rowVirtual, colVirtual);
                        }
                    }
                }

                // TODO : Cette partie ne marche pas, il y a pleins de chiffre aléatoire. (Verifier si la récuperation des informations des cases (Ligne 207) marche correctement). 
                
                // Trouver une seule occurence dans les possibilitées des cubes pour la remplir
                for (int colCube = 0; colCube < _div; colCube++)
                {
                    for (int rowCube = 0; rowCube < _div; rowCube++)
                    {
                        for (int chiffre = 0; chiffre < _size; chiffre++)
                        {
                            if (possibiliteUniqueCub[colCube, rowCube, chiffre, 0] == 1)
                            {
                                KeyValuePair<int, int> coord = SudokuUtils.FindCube(_div, _possibiliteCase, rowCube, colCube, chiffre + 1);
                                if (coord.Key != -1 && coord.Value != -1 && !Changes.Contains(coord.Key + ":" + coord.Value))
                                    MakeChange(chiffre + 1, coord.Key, coord.Value);
                            }
                        }
                    }
                }

                if (_changement == 0)
                {
                    _exitedWithoutAll = true;
                    return;
                }
            }
        }
        
        /* --------------------------
         *
         *
         *          Static
         * 
         * 
         * ------------------------- */
        
        /// <summary>
        /// Fonction qui résout le sudoku donné
        /// </summary>
        /// <param name="values">Tableau des données</param>
        /// <returns>Une instance Sudoku résolue ou presque</returns>
        public static SudokuApi Resolve(int[,] values)
        {
            SudokuApi sudoku = new SudokuApi(values);
            sudoku.Init();
            sudoku.Resolve();
            return sudoku;
        }
    }
}
